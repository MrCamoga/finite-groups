package com.camoga.permutations;
import java.util.ArrayList;
import java.util.Arrays;

public class Permutation {
	
	//Generic determinant
	//Matrix determinant by permutations
	//Print permutations
	// cycle decomposition
	
	//TODO matrix determinant by adjuncts
	
	public static void main(String[] args) {				
		printDeterminant(perm(6, 1, true));
		printPermutations(perm(5, 1, false));
	}
	
	public static int[] permute(int[] a, int[] b) {
		if(a.length!=b.length) throw new RuntimeException();
		int[] c = new int[a.length];
		for(int i = 0; i < a.length; i++) {
			c[i] = b[a[i]];
		}
		return c;
	}
	
	public static int[][] cycledecomposition(int[] perm) {
		ArrayList<Integer> perm2 = new ArrayList<>();
		Arrays.stream(perm).forEach((int d) -> perm2.add(d));
		
		int cyclecount = 0;
		boolean cyclestarted = false;
		
		ArrayList<ArrayList<Integer>> disjointcycles = new ArrayList<>();
		
		int i = 0;
		int j = 0;
		int offset = 0;
		
		while(perm2.size() > 0) {
			if(!cyclestarted) {
				i = perm2.get(0);
				cyclestarted = true;
				if(i > 1) {
					disjointcycles.add(new ArrayList<Integer>());
					disjointcycles.get(cyclecount).add(perm2.get(0)+offset);
					j = i;
					i--;
				} else {
					cyclestarted = false;
					i = 0;
				}
				perm2.remove(0);
			} else {
				if(i > 0) {
					disjointcycles.get(cyclecount).add(perm2.get(i-1)+offset);
					int k = perm2.get(i-1);
					perm2.remove(i-1);
					if(k >= j) i = k-1;
					else i = k;
					j = k;
				} else {
					cyclestarted = false;
					cyclecount++;
					i = 0;
					continue;
				}				
			}
			
			offset++;
			
			for(int k = 0; k < perm2.size(); k++) {
				perm2.set(k, perm2.get(k)-1);
			}
		}
		
		int[][] cycles = new int[disjointcycles.size()][];
		
		for(int r = 0; r < disjointcycles.size(); r++) {
			cycles[r] = new int[disjointcycles.get(r).size()];
			for(int s = 0; s < cycles[r].length; s++) {
				cycles[r][s] = disjointcycles.get(r).get(s);
			}
		}
		
		return cycles;
	}
	
	public static Double computeDeterminant(double[][] matrix) {
		for(int i = 0; i < matrix.length; i++) {
			if(matrix.length != matrix[i].length) return null;
		}
		double sum = 0;
		int[][] perms = perm(matrix.length, 1, true);
		for(int i = 0; i < perms.length; i++) {
			double product = signature(perms[i]);
			for(int j = 0; j < perms[0].length; j++) {
				product *= matrix[j][Math.abs(perms[i][j])-1];		
			}
			sum += product;
		}
		return sum;
	}

	/**
	 * @param array de elementos a permutar
	 * @param sign signatura
	 * @return permutaciones de n
	 */
	public static int[][] perm(int[] array, boolean sign) {
		int n = array.length;
		
		if(n == 1) return new int[][] {array};
		int fact = fact(n);
		
		int[][] permutations = new int[fact][n]; //Array con todas las permutaciones de n elementos
		for(int i = 0; i < n; i++) {
			int[][] smallPerms = perm(removeElement(array, i), sign);	//Calculamos las permutaciones de n menos el i-esimo elemento
			
			for(int j = 0; j < smallPerms.length; j++) {
				//Llevamos el elemento i-esimo a la primera posicion y multiplicamos por la signatura
				//Ademas multiplicamos por la signatura de la permutacion de n-1 elementos para que quede almacenada en el primer elemento
				permutations[i*fact/n+j][0] = array[i];
				if(sign) permutations[i*fact/n+j][0] *= (i%2==1 ? -1:1)*(smallPerms[j][0] < 0 ? -1:1);
				for(int k = 1; k < n; k++) {
					permutations[i*fact/n+j][k] = Math.abs(smallPerms[j][k-1]);
				}
			}
		}
		
		return permutations;
	}
	
	
	public static int[][] perm(int n, int nst, boolean sign) {
		int[] array = new int[n];
		for(int i = 0; i < array.length; i++) array[i] = nst+i;
		return perm(array, sign);
	}
	
	public static int[] removeElement(int[] array, int i) {
		int[] result = new int[array.length-1];
		int j = 0;
		for(int k = 0; k < array.length; k++) {
			if(k == i) {
				j = -1;
				continue;
			}
			result[k+j] = array[k];
		}
		return result;
	}
	
	public static void printDeterminant(int[][] perms) {
		for(int i = 0; i < perms.length; i++) {
			System.out.print(signature(perms[i]) == -1 ? " - ":" + ");
			for(int j = 0; j < perms[0].length; j++) {
				System.out.print("a"+(j+1)+Math.abs(perms[i][j])+"");
			}
		}
	}
	
	public static int signature(int[] perm) {
		return (perm[0] < 0 ? -1:1);
	}
	
	public static int fact(int n) {
		if(n == 0) return 1;
		else return n*fact(n-1);
	}

	public static void printPermutations(int[][] perms) {
		for(int[] i : perms) {
			System.out.print(Arrays.toString(i) + ": ");
			System.out.print(Arrays.deepToString(cycledecomposition(i)));
			System.out.println();
		}
	}
}