package com.camoga.group;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import javax.swing.JFrame;

import com.camoga.permutations.Permutation;

public class Groups extends Canvas {
	
	int WIDTH = 720, HEIGHT = 720;
	BufferedImage image;
	public Groups() {
		JFrame f = new JFrame("Cayley tables");
		setSize(WIDTH,HEIGHT);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(this);
		f.pack();
		f.setVisible(true);
		f.setLocationRelativeTo(null);
		
		
		int[][] A = cyclicGroup(4);
		int[][] B = cyclicGroup(2);
		int[][] C = cyclicGroup(3);
		int[][] Z42 = directProduct(A,B);
		int[][] Z422 = directProduct(Z42, B);
		int[][] Z4222 = directProduct(Z422, B);
		int[][] Z42222 = directProduct(Z4222, B);
		int[][] Z422222 = directProduct(Z42222, B);
//		int[][] G = Z422222;
		int[][] G = symmetricGroup(5);
		
		
		
		int groupSIZE = G.length;
		image = new BufferedImage(groupSIZE, groupSIZE, BufferedImage.TYPE_INT_RGB);
		int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		
		int[] colors = new int[groupSIZE];
		for(int i = 0; i < colors.length; i++) {
			colors[i] = Color.HSBtoRGB(0.75f*i/(colors.length-1), 1.0f, 1.0f);
		}

		for(int g = 0; g < colors.length; g++) {
			for(int k = 0; k < colors.length; k++) {
				pixels[k+g*colors.length] = colors[G[g][k]];				
			}
		}
		
		createBufferStrategy(3);
		while(true) {
			render();		
		}
	}
	
	public int[][] cyclicGroup(int n) {
		int[][] H = new int[n][n];
		for(int g = 0; g < H.length; g++) {
			for(int k = 0; k < H[g].length; k++) {
				H[g][k] = (g+k)%n;
			}
		}
		return H;
	}
	
	public int[][] symmetricGroup(int n) {
		int[][] perms = Permutation.perm(n, 0, false);
		int[][] G = new int[perms.length][perms.length];
		
		for(int g = 0; g < G.length; g++) {
			for(int k = 0; k < G.length; k++) {
				int[] c = Permutation.permute(perms[g], perms[k]);
				G[g][k] = getIndex(perms,c);
			}
		}
		return G;
	}
	
	public int[][] alternatingGroup(int n) {
		int[][] perms = Permutation.perm(n, 1, true);
		int[][] alter = new int[perms.length/2][perms[0].length];
		int j = 0;
		for(int i = 0; i < perms.length; i++) {
			if(perms[i][0] >= 0) {
				for(int k = 0; k < perms[i].length; k++) {
					alter[j][k] = Math.abs(perms[i][k]-1);
				}
				j++;
			}
		}
		int[][] G = new int[alter.length][alter.length];
		
		for(int g = 0; g < G.length; g++) {
			for(int k = 0; k < G.length; k++) {
				int[] c = Permutation.permute(alter[g], alter[k]);
				G[g][k] = getIndex(alter,c);
			}
		}
		return G;
	}

	//< a,x | a^n=x^2=e, xax^-1 = a^-1> 
	public int[][] dihedralGroup(int n) {
		int[][] G = new int[2*n][2*n];
		
		for(int g = 0; g < G.length; g++) {
			for(int k = 0; k < G[g].length; k++) {
				if(g < n) {
					G[g][k] = (k+g)%n + (int)(k/(double)n)*n;
				} else {
					G[g][k] = (((g+(n-1)*(k)))%n + (int)(k/(double)n)*n + n)%(2*n);
				}
			}
		}
		
		return G;
	}
	
	private int getIndex(int[][] perms, int[] perm) {
		for(int j = 0; j < perms.length; j++) {
			for(int k = 0; k < perm.length; k++) {
				if(perms[j][k] != perm[k]) break;
				if(k == perm.length-1) return j;
			}
		}
		return -1;
	}
	
	public int[][] directProduct(int[][] A, int[][] B) {
		int[][] G = new int[A.length*B.length][A.length*B.length];
		for(int y = 0; y < B.length; y++) {
			for(int x = 0; x < B[0].length; x++) {
				for(int g = 0; g < A.length; g++) {
					for(int k = 0; k < A.length; k++) {
						G[g+y*A.length][k+x*A.length] = (A[g][k]+(B[y][x]*A.length))%G.length;
					}
				}
			}
		}
		return G;
	}
	
	public void render() {
		BufferStrategy b = getBufferStrategy();
		
		Graphics g = b.getDrawGraphics();
		
		g.drawImage(image, 0, 0, WIDTH, HEIGHT, null);
		g.dispose();
		b.show();
	}
	
	public static void main(String[] args) {
		new Groups();
	}
}